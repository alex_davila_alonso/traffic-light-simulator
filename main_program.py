import random

import paho.mqtt.client as mqtt
import requests
import json
import time
from CollisionMicroservice import collision_api
from threading import Thread, Event, Lock
from queue import Queue
import time
from picamera2.encoders import H264Encoder
from picamera2 import Picamera2, Preview
import os
from datetime import datetime
import requests
from security_encryption import *
import base64
from collision_client import start_collision_client
from weather_client import start_weather_client
                
def main_program():
    collision_thread = Thread(target=start_collision_client)
    weather_thread = Thread(target=start_weather_client)
    
    collision_thread.start()
    weather_thread.start()
    
if __name__ == "__main__":
    main_program()