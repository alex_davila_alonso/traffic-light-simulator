from flask import Flask, jsonify, request
import random
from datetime import datetime, timedelta
from flask_jwt_extended import create_access_token, JWTManager

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'jwt_secret_key'
jwt = JWTManager(app)

username = "sample-username"
password = "sample-password"
last_login = datetime.now()

postalCodeList = ['J3A1H7', 'J9X2E7', 'J9X2E6', 'J6J0A2', 'H8T0A1']
weatherType = ['snowfall', 'rain', 'sunny', 'cloudy']
weatherIntensity = ['heavy', 'medium', 'light', 'n/a']

@app.route('/api/weather/<postal_code>', methods=['GET', 'POST'])
def get_postal_code(postal_code):
  if postal_code in postalCodeList and postal_code is not None:
    randomTemp = random.randrange(-40, 40)
    randomWeather = random.choice(weatherType)
    randomIntensity = generateRandomIntensity(randomWeather)
    today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    json_data = {
      "postal_code": postal_code,
      "temperature": randomTemp,
      "date": today,
      "condition": {
        "type": randomWeather,
        "intensity": randomIntensity
      }
    }
    
    return jsonify(json_data)
  else:
    return jsonify({"ERROR": "Invalid postal code"}), 404
  

@app.route('/login', methods=['GET', 'POST'])
def login():
    with app.app_context():
        data = request.get_json()
        json_username = data.get('Username')
        json_password = data.get('Password')

        # Validate the request body has username and password
        if not json_username or not json_password:
            return jsonify({'error': 'Username and Password are required!'}), 400

        if not json_password == password:
            return jsonify({'error': 'Invalid Password'}), 401

        # Check the password
        if not json_username == username:
            return jsonify({'error': 'Invalid Username'}), 401

        # Generate access token
        access_token = create_access_token(identity=username, expires_delta=timedelta(hours=1))

        return jsonify(access_token=access_token), 200




def generateRandomIntensity(randomWeather):
  if randomWeather == 'snowfall' or randomWeather == 'rain':
    while True:
        randomIntensity = random.choice(weatherIntensity)
        if randomIntensity != 'n/a':
          return randomIntensity
  else:
    return 'n/a'
  
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002, debug=True)