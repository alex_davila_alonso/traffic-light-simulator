import pytest
import requests
from weather_api import app, weatherIntensity, weatherType
from datetime import datetime

@pytest.fixture
def client():
  with app.test_client() as client:
    yield client
    
def test_api_valid(client):
  response = client.get('/api/weather/J3A1H7')
  data = response.json
  today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  print(data)
  
  assert response.status_code == 200
  assert data['postal_code'] == 'J3A1H7'
  assert data['temperature'] >= -40 and data['temperature'] <= 40
  assert data['condition']['type'] in weatherType
  assert data['condition']['intensity'] in weatherIntensity
  assert "date" in data.keys()
  assert data['date'] == today
  
def test_api_invalid_postal_code(client):
  response = client.get('/api/weather/ERROR')
  
  assert response.status_code == 404
  assert response.json == {"ERROR": "Invalid postal code"}
  
def test_api_missing_postal_code(client):
  response = client.get('/api/weather/')
  
  assert response.status_code == 404
  assert response.json == None
  

def test_jwt_success(client):
  credentials = {'Username': 'sample-username', 'Password': 'sample-password'}
  response = client.post('/login', json=credentials)
  
  assert response.status_code == 200
  assert 'access_token' in response.json
  
def test_missing_credentials(client):
  response = client.post('/login', json={})
  assert response.status_code == 400
  
def test_invalid_password(client):
  credentials = {'Username':'sample-username', 'Password':'wrong-password'}
  response = client.post('login', json=credentials)
  assert response.status_code == 401
  
def test_invalid_username(client):
  credentials = {'Username': 'wrong-username', 'Password': 'sample-password'}
  response = client.post('login', json=credentials)
  assert response.status_code == 401
  