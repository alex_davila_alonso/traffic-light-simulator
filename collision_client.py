import random
import paho.mqtt.client as mqtt
import requests
import json
import time
from CollisionMicroservice import collision_api
from threading import Thread, Event, Lock
from queue import Queue
import time
from picamera2.encoders import H264Encoder
from picamera2 import Picamera2, Preview
import os
import datetime
import requests
from security_encryption import *
import base64
from traffic_light import TrafficLight
from security_encryption import sign
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa


MQTT_BROKER = "10.172.12.44"
MQTT_PORT = 1883
DATA_TOPIC = "collision/data"
SIGNATURE_TOPIC = "signature/collision"

# Load the private key
with open('private_key.pem', 'rb') as f:
    private_key = serialization.load_pem_private_key(f.read(), password=None)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

USERNAME = 'sample-username'
PASSWORD = 'sample-password'
API_LOGIN_ENDPOINT = 'http://10.172.12.44:5001/login'

client = mqtt.Client()
client.on_connect = on_connect
client.connect(MQTT_BROKER, MQTT_PORT, 60)


last_token_retrieval = None
token_expiry = None
current_token = None


def get_saved_token():
    global last_token_retrieval, token_expiry, current_token
    if not last_token_retrieval or datetime.datetime.now() >= token_expiry:
        response = requests.post(API_LOGIN_ENDPOINT, json={'Username': USERNAME, 'Password': PASSWORD})

        if response.status_code == 200:
            token = response.json().get('access_token')
            last_token_retrieval = datetime.datetime.now()
            token_expiry = last_token_retrieval + datetime.timedelta(hours=1)
            current_token = token
            print(f"Successfully logged in!")
        else:
            print(f"Failed to login: {response.status_code} - {response.content}")
            current_token = None
    return current_token

def start_collision_client():
    client.loop_start()

    picam2 = Picamera2()

                    
    traffic_light = TrafficLight(picam2, client, DATA_TOPIC, SIGNATURE_TOPIC, private_key, 5)

    try:
    # Fetch data from the Flask API and publish to the MQTT broker
        while True:
            token = get_saved_token()
            headers = {
                'Authorization': f'Bearer {token}'
            }
            postal_code = random.choice(collision_api.random_postal_codes)
            response = requests.get(f'http://10.172.12.44:5001/api/collision/{postal_code}', headers=headers)
            if response.status_code == 200:
                data = response.json()
                timestamp = int(time.time())
                data['timestamp'] = timestamp
                
                print("Sending Collision Data:", json.dumps(data))

            # Photo implementation
            
            time.sleep(1)  # Poll every second
            
            try:
                photoFolder = os.mkdir('./CriminalPhotosFolder')
            except FileExistsError:
                pass
            
            try:
                # Thread for camera picture when infraction happens
                traffic_light.data = data
                trafficInfractionThread = Thread(target=traffic_light.runSimulation)
                trafficInfractionThread.start()
                
                traffic_light_simulation_thread = Thread(target=traffic_light.trafficLightSimulation)
                traffic_light_simulation_thread.start()
            except KeyboardInterrupt:
                print("\n Program interrupted by user. Exiting ...")
                traffic_light.stop_threads()
    finally:
        client.loop_stop()