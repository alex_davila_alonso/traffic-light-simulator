from datetime import datetime, timedelta
from flask import Flask, jsonify, request
from flask_jwt_extended import create_access_token, jwt_required, JWTManager
import random

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'jwt_secret_key'
jwt = JWTManager(app)

username = "sample-username"
password = "sample-password"
last_login = datetime.now()

random_postal_codes = ["P1S P4S", "L4R P4D", "A1F P9S", "D3A C9A", "K1R P4S"]

detection_type = ["motion", "collision"]
detection_value = ["true", "false"]

@jwt_required()
@app.route('/api/collision/<string:postal_code>')
# @jwt_required()
def get_collisions(postal_code):
    if postal_code not in random_postal_codes:
        return jsonify({"ERROR": "Invalid postal code"}), 404

    json_value = random.choice(detection_value)
    json_type = random.choice(detection_type)

    detection = {
        "type": json_value,
        "value": json_type,
    }

    current_time = datetime.now()

    collision_json = {
        "postal_code": postal_code,
        "detection": detection,
        "datetime": current_time.strftime("%Y-%m-%d %H:%M:%S"),
    }

    return jsonify(collision_json)


@app.route('/login', methods=['GET', 'POST'])
def login():
    with app.app_context():
        data = request.get_json()
        json_username = data.get('Username')
        json_password = data.get('Password')

        # Validate the request body has username and password
        if not json_username or not json_password:
            return jsonify({'error': 'Username and Password are required!'}), 400

        if not json_password == password:
            return jsonify({'error': 'Invalid Password'}), 401

        # Check the password
        if not json_username == username:
            return jsonify({'error': 'Invalid Username'}), 401

        # Generate access token
        access_token = create_access_token(identity=username, expires_delta=timedelta(hours=1))

        return jsonify(access_token=access_token), 200


if __name__ == '__main__':
    app.run(debug=True)
