import pytest
from .collision_api import app, detection_type, detection_value, random_postal_codes
from datetime import datetime, timedelta

@pytest.fixture
def client():
  with app.test_client() as client:
    yield client
    
def test_valid_collision(client):
  response = client.get(f'/api/collision/P1S P4S', headers={'Authorization': 'Bearer valid_token'})
  data = response.json
  today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  
  assert response.status_code == 200
  assert data['postal_code'] == 'P1S P4S'
  assert data['detection']['type'] in detection_value
  assert data['detection']['value'] in detection_type
  assert data['datetime'] == today
  
def test_invalid_collision_detection(client):
  response = client.get(f'/api/collision/ERROR', headers={'Authorization': 'Bearer valid_token'})
  
  assert response.status_code == 404
  assert response.json == {"ERROR": "Invalid postal code"}
  
# Token Test
def test_login_success(client):
  credentials = {'Username': 'sample-username', 'Password': 'sample-password'}
  response = client.post('/login', json=credentials)
  data = response.json
  
  assert response.status_code == 200
  assert 'access_token' in data
  
def test_missing_credentials(client):
  credentials = {}
  response = client.post('/login', json=credentials)
  
  assert response.status_code == 400
  assert response.json == {'error': 'Username and Password are required!'}
  
def test_invalid_password(client):
  credentials = {'Username': 'sample-username', 'Password': 'wrong-password'}
  response = client.post('/login', json=credentials)
  
  assert response.status_code == 401
  assert response.json == {'error': 'Invalid Password'}
  
def test_invalid_username(client):
  credentials = {'Username': 'wrong-username', 'Password': 'sample-password'}
  response = client.post('/login', json=credentials)
  
  assert response.status_code == 401
  assert response.json == {'error': 'Invalid Username'}