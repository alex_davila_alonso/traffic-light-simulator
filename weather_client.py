import datetime
import random
import paho.mqtt.client as mqtt
import requests
import json
import time
from WeatherMicroservice import weather_api
from security_encryption import sign
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa


MQTT_BROKER = "10.172.12.44"
MQTT_PORT = 1883
DATA_TOPIC = "weather/data"
SIGNATURE_TOPIC = "signature/weather"

with open('private_key.pem', 'rb') as f:
    private_key = serialization.load_pem_private_key(f.read(), password=None)


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

USERNAME = 'sample-username'
PASSWORD = 'sample-password'
API_LOGIN_ENDPOINT = 'http://10.172.12.44:5002/login'


last_token_retrieval = None
token_expiry = None
current_token = None


def get_saved_token():
    global last_token_retrieval, token_expiry, current_token
    if not last_token_retrieval or datetime.datetime.now() >= token_expiry:
        response = requests.post(API_LOGIN_ENDPOINT, json={'Username': USERNAME, 'Password': PASSWORD})

        if response.status_code == 200:
            token = response.json().get('access_token')
            last_token_retrieval = datetime.datetime.now()
            token_expiry = last_token_retrieval + datetime.timedelta(hours=1)
            current_token = token
            print(f"Successfully logged in!")
        else:
            print(f"Failed to login: {response.status_code} - {response.content}")
            current_token = None
    return current_token

def start_weather_client():
    client = mqtt.Client()
    client.on_connect = on_connect
    client.connect(MQTT_BROKER, MQTT_PORT, 60)
    client.loop_start()

    try:
        while True:
            token = get_saved_token()
            if token:
                headers = {
                    'Authorization': f'Bearer {token}'
                }
                postal_code = random.choice(weather_api.postalCodeList)
                response = requests.get(f"http://10.172.12.44:5002/api/weather/{postal_code}", headers=headers)
                if response.status_code == 200:
                    data = response.json()
                    timestamp = int(time.time())
                    data['timestamp'] = timestamp

                    print("Sending Weather Data:", json.dumps(data))

                    # Convert data to bytes and sign
                    data_bytes = json.dumps(data).encode('utf-8')
                    signature = sign(data_bytes, private_key)

                    # Publish the data to DATA_TOPIC
                    client.publish(DATA_TOPIC, json.dumps(data))
                    # Publish the signature to SIGNATURE_TOPIC
                    client.publish(SIGNATURE_TOPIC, json.dumps({"timestamp": timestamp, "signature": signature.hex()}))
                    print("Weather Data Published")

            time.sleep(1)
    except Exception as e:
        print(f"Error happened here: {e}")
        pass
    finally:
        client.loop_stop()