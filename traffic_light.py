import random

import paho.mqtt.client as mqtt
import requests
import json
import time
from CollisionMicroservice import collision_api
from threading import Thread, Event, Lock
from queue import Queue
import time
from picamera2.encoders import H264Encoder
from picamera2 import Picamera2, Preview
import os
from datetime import datetime
import requests
from security_encryption import *
import base64

class TrafficLight:
        def __init__(self, picam2, client, topic, signature_topic, private_key, delay=1):
            self.data = None
            self.picam2 = picam2
            self.client = client
            self.topic = topic
            self.signature_topic = signature_topic
            self.private_key = private_key
            self.time_to_sleep = delay
            self.lightColor = 'blue'
            self.light_color_lock = Lock()
            self.stop_event = Event()
            self.color_change_queue = Queue()
            
        def stop_threads(self):
            self.stop_event.set()
        
        def read_png(self, file_path):
            with open (file_path, "rb") as image_file:
                binary_data = image_file.read()
            return binary_data
        
        def trafficLightSimulation(self):
            while True:
                if self.lightColor == 'green':
                    self.lightColor = 'blue'
                    #print('Traffic light color is BLUE')
                elif self.lightColor == 'red':
                    self.lightColor = 'green'
                    #print('Traffic light color is GREEN')
                elif self.lightColor == 'blue':
                    self.lightColor = 'red'
                    #print('Traffic light color is RED')
                
                self.color_change_queue.put(self.lightColor)
                time.sleep(self.time_to_sleep)
        
        def runSimulation(self):
            try:
                print("Running simulation...")
                photo_taken = False
                
                if self.data is not None:
                    print("Data is not None, processing...")
                    expected_color = self.color_change_queue.get()
                    if self.data['detection']['value'] == 'collision' and self.data['detection']['type'] == 'true':
                        self.TakePicture(self.picam2)
                        photo_taken = True
                    elif expected_color == 'red' and self.data['detection']['value'] == 'motion' and self.data['detection']['type'] == 'true':
                        self.TakePicture(self.picam2)
                        photo_taken = True
                    
                    if not photo_taken:
                        self.data["photo"] = "No photo taken"
                    
                    self.publishData()
            except Exception as e:
                print(f"Error happened here: {e}")
                

                
        def publishData(self):
            print("before check")
            if self.data is not None:
                print("after check passed")
                # Convert data to bytes and sign
                data_bytes = json.dumps(self.data).encode('utf-8')
                signature = sign(data_bytes, self.private_key)
                self.client.publish(self.topic, json.dumps(self.data))
                self.client.publish(self.signature_topic, json.dumps({"timestamp": self.data['timestamp'], "signature": signature.hex()}))
             

        def TakePicture(self, picam2):
            with self.light_color_lock:
                now = datetime.now()
                timeStamp = now.strftime('%d-%m-%Y %H:%M:%S')
                
                photo_config = picam2.create_preview_configuration()
                picam2.configure(photo_config)
                picam2.start()
                output = f"criminal_photo_{timeStamp}.jpg"
                picam2.capture_file(f"./CriminalPhotosFolder/{output}")
                picam2.stop()
                print("Traffic offense recorded")
                binary_data = self.read_png(f"./CriminalPhotosFolder/{output}")
                base64_data = base64.b64encode(binary_data).decode('utf-8')
                self.data["photo"] = base64_data
                
                os.remove(f"./CriminalPhotosFolder/{output}")