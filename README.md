
# Team Assignment 2

## Overview
This project demonstrates a real-time MQTT-based dashboard that processes and displays collision and weather data. It uses an MQTT broker for message passing between a set of clients and a web-based dashboard built with Dash.

## Prerequisites
- Python 3.6 or later
- Docker
- MQTT Broker (e.g., Mosquitto)

## Setup and Installation

### 0. Run mosquitto.conf file to setup mosquitto broker
```bash
mosquitto -c ./mosquitto/config/mosquitto.conf
```

### 1. Generating RSA Keys
First, generate RSA key pairs used for signing and verifying messages:
```bash
python key_generator.py
```
This script will create `private_key.pem` and `public_key.pem` files.

### 2. Running Docker Containers for APIs
Run Docker containers for both the Collision and Weather microservices:
```bash
cd CollisionMicroservice
sudo docker build -t collision-api .
sudo docker run -d -p 5001:5001 collision-api

cd ..
cd WeatherMicroservice
sudo docker build -t weather-api .
sudo docker run -d -p 5002:5002 weather-api
```

### 3. Starting MQTT Clients
Run the MQTT clients for both collision and weather data:
```bash
cd ..
python main_program.py
```

### 4. Launching the Dashboard
Finally, start the Dash dashboard:
```bash
python dashboard.py
```

## Usage
Once all components are up and running, the dashboard will display real-time data about weather conditions and collision detections. This data is updated live as the MQTT clients publish new messages.

## Troubleshooting
- Ensure all dependencies are installed and Docker containers are running.
- Check MQTT broker connectivity.
- Verify the RSA keys are correctly generated and accessible to the clients and the dashboard.
