import unittest
import json
import os
import sys
from unittest.mock import patch
from security_encryption import verify_signature, sign, generate_key_pair
from cryptography.hazmat.primitives import serialization

class VerifySignatureWeather(unittest.TestCase):
  def test_valid_signature(self):
    mock_private_key, mock_public_key = generate_key_pair()
    
    check_data = {"key": 123}
    
    data = json.dumps(check_data).encode('utf-8')
    signed_data = sign(data, mock_private_key)
    
    public_key_bytes = mock_public_key.public_bytes(
      encoding=serialization.Encoding.PEM,
      format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    verify_signature_data = verify_signature(signed_data, data, public_key_bytes)
    
    self.assertEqual(verify_signature_data, "Message verified")
    
  def test_invalid_signature(self):
    mock_private_key, mock_public_key = generate_key_pair()
    
    check_data = {"key": 123}
    data = json.dumps(check_data).encode('utf-8')
    
    signed_data = sign(data, mock_private_key)
    
    bad_signature = bytearray(signed_data)
    bad_signature[0] = (bad_signature[0] + 1) % 256
    byte_bad_signature = bytes(bad_signature)
    
    public_key_bytes = mock_public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    
    verify_signature_data = verify_signature(byte_bad_signature, data, public_key_bytes)
    
    self.assertEqual(verify_signature_data, "Invalid signature/message or public key")