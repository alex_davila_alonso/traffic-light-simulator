import queue

import dash
from dash import html
import dash_core_components as dcc
import dash_daq as daq
from dash.dependencies import Input, Output
import paho.mqtt.client as mqtt
import json
import threading
from security_encryption import verify_signature
import base64

# Load keys from the file system
with open('public_key.pem', 'rb') as f:
    public_key = f.read()

# MQTT setup
MQTT_BROKER = "10.172.12.44"
MQTT_PORT = 1883
DATA_TOPICS = ["collision/data", "weather/data"]
SIGNATURE_TOPICS = ["signature/collision", "signature/weather"]


# Queues for data
collision_data_queue = queue.Queue()
weather_data_queue = queue.Queue()

# Initialize MQTT client
mqtt_client = mqtt.Client()
# Function to process MQTT messages
def process_mqtt_messages():
    data_buffers = {"collision/data": [], "weather/data": []}
    signatures = {"signature/collision": {}, "signature/weather": {}}

    def on_message(client, userdata, msg):
        print(f"Received message on topic: {msg.topic}")

        if msg.topic in DATA_TOPICS:
            temp_data = json.loads(msg.payload.decode())
            if msg.topic == "collision/data":
                print("Received Collision Data:", temp_data)
            elif msg.topic == "weather/data":
                print("Received Weather Data:", temp_data)
            data_buffers[msg.topic].append(temp_data)
        elif msg.topic in SIGNATURE_TOPICS:
            signature_data = json.loads(msg.payload.decode())
            signatures[msg.topic][signature_data['timestamp']] = signature_data['signature']
        process_buffered_messages()

    def process_buffered_messages():
        for topic in DATA_TOPICS:
            signature_topic = "signature/" + topic.split('/')[0]
            data_buffer = data_buffers[topic]
            signature_dict = signatures[signature_topic]
            for data in data_buffer[:]:
                data_timestamp = data['timestamp']
                if data_timestamp in signature_dict:
                    signature = signature_dict[data_timestamp]
                    # TODO: Problem happens here
                    result = verify_signature(bytes.fromhex(signature), json.dumps(data).encode('utf-8'), public_key)
                    print(f"Verification result for topic {topic}: {result}")
                    if result == "Message verified":
                        print(f"Verified data for topic {topic}: {data}")
                        if topic == "collision/data":
                            collision_data_queue.put(data)
                            print("Added to collision queue")
                        elif topic == "weather/data":
                            weather_data_queue.put(data)
                            print("Added to weather queue")
                        data_buffer.remove(data)

    mqtt_client.on_message = on_message
    mqtt_client.subscribe(DATA_TOPICS[0])
    mqtt_client.subscribe(DATA_TOPICS[1])
    mqtt_client.subscribe(SIGNATURE_TOPICS[0])
    mqtt_client.subscribe(SIGNATURE_TOPICS[1])
    mqtt_client.loop_forever()


# Initialize MQTT client and thread
mqtt_client.connect(MQTT_BROKER, MQTT_PORT, 60)
thread = threading.Thread(target=process_mqtt_messages)
thread.start()

# Dash setup
app = dash.Dash(__name__)
server = app.server

# Define the initial layout
app.layout = html.Div([
    html.Div(id='live-update-text'),
    dcc.Interval(
        id='interval-component',
        interval=2*1000,  # in milliseconds
        n_intervals=0
    )
])


# Callback to update the live-update-text component
@app.callback(Output('live-update-text', 'children'),
              [Input('interval-component', 'n_intervals')])
def update_layout(n):
    print("Callback triggered")
    latest_collision_data = collision_data_queue.get() if not collision_data_queue.empty() else None
    latest_weather_data = weather_data_queue.get() if not weather_data_queue.empty() else None

    print(f"Latest Collision Data: {latest_collision_data}")
    print(f"Latest Weather Data: {latest_weather_data}")

    # Check if we have collision data to process
    if latest_collision_data:
        base64_encoded_image = decode_image(latest_collision_data)
        detected = bool(base64_encoded_image)
        indicator_color = "#FF5E5E" if detected else "#808080"
        collision_data_div = html.Div([
            html.H2('Collision Data'),
            html.Div([
                html.Img(src='/assets/collision-icon.png', style={'height': '30px'}),
                html.Span('Postal Code: {}'.format(latest_collision_data['postal_code'])),
            ], style={'display': 'flex', 'alignItems': 'center'}),
            html.Div('Detection Type: {}'.format(latest_collision_data['detection']['value'])),
            daq.Indicator(
                id='indicator',
                value=detected,
                color=indicator_color,
                label="Detected",
                labelPosition="bottom"
            ),
            *(
                [html.Div([
                html.Img(src=f'data:image/jpeg;base64,{base64_encoded_image}', width=500, height=500)
                ])] if base64_encoded_image else [
                    html.H3('No Detection', style={"color": "red"})
                ]
            ),
            html.Div('Time of Detection: {}'.format(latest_collision_data['datetime']))
        ])
    else:
        collision_data_div = html.Div('No new collision data available.')

    # Process weather data similarly
    if latest_weather_data:
        weather_data_div = html.Div([
            html.H2('Weather Data'),
            html.Div([
                daq.Thermometer(
                    id='temperature-1',
                    value=latest_weather_data['temperature'],
                    min=-40,
                    max=40,
                    style={
                        'margin-left': '3%',
                        'margin-bottom': '1%',
                        'display': 'inline-block'
                    }
                )
            ]),
            html.Div('Postal Code: {}'.format(latest_weather_data['postal_code'])),
            html.Div('Weather Condition Type: {}'.format(latest_weather_data['condition']['type'])),
            html.Div('Weather Condition Intensity: {}'.format(latest_weather_data['condition']['intensity'])),
            html.Div('Date: {}'.format(latest_weather_data['date']))
        ])
    else:
        weather_data_div = html.Div('No new weather data available.')

    return html.Div([collision_data_div, weather_data_div])

def decode_image(data):
    try:
        base64_photo = data.get("photo")
        
        if base64_photo and base64_photo != 'No photo taken':
            binary_photo = base64.b64decode(base64_photo)
            base64_encoded_image = base64.b64encode(binary_photo).decode('utf-8')
            return base64_encoded_image
        
        return None
    except Exception as e:
        print(f"Error found: {e}")
    
if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=8050)
