from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.primitives import serialization


def generate_key_pair():
  key_size = 2048
  
  private_key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=key_size
  )
  public_key = private_key.public_key()
  return private_key, public_key

def encrypt(message, public_key):
  return public_key.encrypt(
    message,
    padding.OAEP(
      mgf=padding.MGF1(algorithm=hashes.SHA256()),
      algorithm=hashes.SHA256(),
      label=None
    )
  )

def decrypt(message_encrypted, private_key):
  try:
    message_decrypted = private_key.decrypt(
      message_encrypted,
      padding.OAEP(
        mgf=padding.MGF1(algorithm=hashes.SHA256()),
        algorithm=hashes.SHA256(),
        label=None
      )
    )
    return f"Decrupted message: {message_decrypted}"
  except ValueError:
    return "Failed to decrypt"
  
def sign(message, private_key):
  return private_key.sign(
    message,
    padding.PSS(
      mgf=padding.MGF1(hashes.SHA256()),
      salt_length=padding.PSS.MAX_LENGTH
    ),
    hashes.SHA256()
  )

def verify_signature(signature, message, public_key_bytes):
  try:
    # Deserialize the public key from bytes
    public_key = serialization.load_pem_public_key(public_key_bytes)

    public_key.verify(
      signature,
      message,
      padding.PSS(
        mgf=padding.MGF1(hashes.SHA256()),
        salt_length=padding.PSS.MAX_LENGTH
      ),
      hashes.SHA256()
    )
    return "Message verified"
  except InvalidSignature:
    return "Invalid signature/message or public key"
  